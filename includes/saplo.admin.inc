<?php

/**
 * @file
 * Administration page callbacks and forms.
 */

/**
 * Settings form for the Saplo module.
 */
function saplo_admin_settings($form, $form_state) {
  $form['authentication'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentication'),
  );

  $form['authentication']['saplo_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('saplo_api_key', ''),
  );

  $form['authentication']['saplo_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('saplo_api_key', ''),
  );

  return system_settings_form($form);
}
